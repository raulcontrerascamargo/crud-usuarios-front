import { Injectable } from '@angular/core';
import { Socket, io } from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class WebsocketService {
  constructor() {}

  private socket: Socket = io('http://localhost:3000');

  connect(): void {
    this.socket.on('connect', () => {
      console.log('Conexión WebSocket establecida');
    });

    this.socket.on('disconnect', () => {
      console.log('Conexión WebSocket cerrada');
    });
  }

  sendEvent(eventName: string, data: any): void {
    this.socket.emit(eventName, data);
  }

  onEvent(eventName: string): Observable<any> {
    return new Observable<any>((observer) => {
      this.socket.on(eventName, (data) => {
        observer.next(data);
      });
    });
  }

  disconnect(): void {
    this.socket.disconnect();
  }
}
