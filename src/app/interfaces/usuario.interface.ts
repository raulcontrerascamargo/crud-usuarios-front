export interface UsuarioInterface {
  id: number;
  nombre: string;
  apellido: string;
  edad: number;
  telefono: string;
  correo: string;
}
