import { Component, OnInit, OnDestroy, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { UsuarioInterface } from 'src/app/interfaces/usuario.interface';
import { UsuarioService } from 'src/app/services/usuario.service';
import { WebsocketService } from 'src/app/services/websocket.service';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: './lista-usuarios.component.html',
  styleUrls: ['./lista-usuarios.component.css'],
})
export class ListaUsuariosComponent implements OnInit, OnDestroy {
  constructor(
    private readonly usuarioService: UsuarioService,
    private webSocketService: WebsocketService,
    private router: Router
  ) {}

  listaUsuarios: UsuarioInterface[] = [];

  mensajeNotificacion: string = '';
  mostrarNotificacion: boolean = false;

  ngOnInit(): void {
    this.obtenerListadoUsuarios();

    // Establecer la conexión WebSocket
    this.webSocketService.connect();

    // Escuchar un evento del servidor
    this.webSocketService.onEvent('userDeleted').subscribe((data) => {
      console.log('Evento del servidor recibido:', data);

      this.obtenerListadoUsuarios();
    });
  }

  obtenerListadoUsuarios() {
    this.usuarioService.getListadoUsuarios().subscribe((data) => {
      this.listaUsuarios = data;
    });
  }

  abrirEditar(idUsuario: number) {
    this.router.navigate(['/editar', idUsuario]);
  }

  eliminarUsuario(idUsuario: number) {
    this.usuarioService.deleteUsuario(idUsuario).subscribe((data) => {
      console.log('data', data);
      this.mostrarNotificacion = true;
      this.mensajeNotificacion = 'Se elimino el usuario';
      setTimeout(() => {
        this.mostrarNotificacion = false;
      }, 3000);
    });
  }

  ngOnDestroy(): void {
    this.webSocketService.disconnect();
  }
}
