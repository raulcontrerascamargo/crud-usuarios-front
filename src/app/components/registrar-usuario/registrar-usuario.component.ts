import { Component } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';
import {
  FormGroup,
  FormControl,
  FormGroupDirective,
  FormBuilder,
  Validators,
  AbstractControl,
  ValidationErrors,
} from '@angular/forms';
import { REGEX_NAME, REGEX_PHONE } from 'src/app/environments/regex';

@Component({
  selector: 'app-registrar-usuario',
  templateUrl: './registrar-usuario.component.html',
  styleUrls: ['./registrar-usuario.component.css'],
})
export class RegistrarUsuarioComponent {
  formGroup: FormGroup;
  constructor(
    private readonly usuarioService: UsuarioService,
    private formBuilder: FormBuilder,

  ) {
    this.formGroup = this.formBuilder.group({
      nombre: new FormControl('', [
        Validators.required,
        Validators.pattern(REGEX_NAME),
      ]),
      apellido: new FormControl('', [
        Validators.required,
        Validators.pattern(REGEX_NAME),
      ]),
      edad: new FormControl('', [Validators.required, this.numbersValidator]),
      telefono: new FormControl('', [
        Validators.required,
        this.phoneNumberValidator,
        Validators.pattern(REGEX_PHONE),
      ]),
      correo: new FormControl('', [Validators.required, Validators.email]),
    });
  }


  mostrarNotificacion: boolean = false;


  //valida que el valor sean unicamente números
  numbersValidator(control: AbstractControl): ValidationErrors | null {
    const value = control.value;

    const isNumber = /^\d+$/.test(value);
    return isNumber ? null : { onlyNumbers: true };
  }

  //impide que se puedan escribir caracteres que no sean números
  onInput(event: any): void {
    const inputValue = event.target.value;
    const numbersOnly = inputValue.replace(/[^0-9]/g, '');
    event.target.value = numbersOnly;
  }

  phoneNumberValidator(event: any): ValidationErrors | null {
    // Eliminar guiones antes de enviar el formulario
    const telefonoSinGuiones = event.value.replace(/-/g, '');

    if (telefonoSinGuiones.length === 10 && /^\d+$/.test(telefonoSinGuiones)) {
      return null; // El número de teléfono es válido
    } else {
      return { phoneNumber: true }; // El número de teléfono es inválido
    }
  }

  mensajeErrorNombre(): string {
    if (this.formGroup.get('nombre')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('nombre')?.hasError('pattern')) {
      return 'Nombre invalido';
    }
    return '';
  }

  mensajeErrorApellido(): string {
    if (this.formGroup.get('apellido')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('apellido')?.hasError('pattern')) {
      return 'Apellido invalido';
    }
    return '';
  }

  mensajeErrorEdad(): string {
    if (this.formGroup.get('edad')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('edad')?.hasError('onlyNumbers')) {
      return 'Solo se permiten números';
    }
    return '';
  }

  mensajeErrorTelefono(): string {
    if (this.formGroup.get('telefono')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('telefono')?.hasError('pattern')) {
      return 'Solo se permiten números y guiones';
    }

    return 'El numero es mayor a 10 dígitos';
  }

  mensajeErrorCorreo(): string {
    if (this.formGroup.get('correo')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('correo')?.hasError('email')) {
      return 'correo electrónico invalido';
    }
    return '';
  }

  onSubmit() {
    if (this.formGroup.valid) {
      this.usuarioService.postUsuario(this.formGroup.value).subscribe(
        (data) => {
          console.log('res', data);
          this.mostrarNotificacion = true
          setTimeout(() => {
            this.mostrarNotificacion = false
          }, 3000);
          
        },
        (err) => {
         
        }
      );
    } else {
      console.log('not valid');
    }
  }
}
