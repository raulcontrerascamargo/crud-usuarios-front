import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UsuarioInterface } from '../interfaces/usuario.interface';

@Injectable({
  providedIn: 'root',
})
export class UsuarioService {
  constructor(private http: HttpClient) {}

  getUsuario(idUsuario: number): Observable<UsuarioInterface> {
    return this.http.get<UsuarioInterface>(
      `http://localhost:3000/api/usuarios/${idUsuario}`
    );
  }

  getListadoUsuarios(): Observable<UsuarioInterface[]> {
    return this.http.get<UsuarioInterface[]>(
      'http://localhost:3000/api/usuarios'
    );
  }

  postUsuario(usuario: UsuarioInterface) {
    return this.http.post<UsuarioInterface>(
      'http://localhost:3000/api/usuarios',
      usuario
    );
  }

  putUsuario(idUsuario: number, usuario: UsuarioInterface) {
    return this.http.put<UsuarioInterface>(
      `http://localhost:3000/api/usuarios/${idUsuario}`,
      usuario
    );
  }

  deleteUsuario(idUsuario: number) {
    return this.http.delete(`http://localhost:3000/api/usuarios/${idUsuario}`);
  }
}
