import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appFormatPhoneNumber]',
})
export class FormatPhoneNumberDirective {
  constructor(private el: ElementRef) {}

  @HostListener('input')
  onInput() {
    const phoneNumber = this.el.nativeElement.value;
    const formattedPhoneNumber = this.formatPhoneNumber(phoneNumber);
    this.el.nativeElement.value = formattedPhoneNumber;
  }

  private formatPhoneNumber(phoneNumber: string): string {
    phoneNumber = phoneNumber.replace(/-/g, '');

    if (phoneNumber.length > 3 && phoneNumber.length <= 6) {
      phoneNumber = phoneNumber.slice(0, 3) + '-' + phoneNumber.slice(3);
    } else if (phoneNumber.length > 6) {
      phoneNumber =
        phoneNumber.slice(0, 3) +
        '-' +
        phoneNumber.slice(3, 6) +
        '-' +
        phoneNumber.slice(6);
    }

    return phoneNumber;
  }
}
