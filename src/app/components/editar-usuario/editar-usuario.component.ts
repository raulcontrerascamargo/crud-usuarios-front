import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { REGEX_NAME, REGEX_PHONE } from 'src/app/environments/regex';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-editar-usuario',
  templateUrl: './editar-usuario.component.html',
  styleUrls: ['./editar-usuario.component.css'],
})
export class EditarUsuarioComponent implements OnInit {
  formGroup: FormGroup;
  constructor(
    private readonly usuarioService: UsuarioService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.formGroup = this.formBuilder.group({
      nombre: new FormControl('', [
        Validators.required,
        Validators.pattern(REGEX_NAME),
      ]),
      apellido: new FormControl('', [
        Validators.required,
        Validators.pattern(REGEX_NAME),
      ]),
      edad: new FormControl('', [Validators.required, this.numbersValidator]),
      telefono: new FormControl('', [
        Validators.required,
        this.phoneNumberValidator,
        Validators.pattern(REGEX_PHONE),
      ]),
      correo: new FormControl('', [Validators.required, Validators.email]),
    });
  }

  mostrarNotificacion: boolean = false;

  idUsuario: number = Number(this.route.snapshot.paramMap.get('idUsuario'));

  ngOnInit(): void {
    this.usuarioService.getUsuario(this.idUsuario).subscribe((data) => {
      console.log('data', data);
      this.formGroup.patchValue(data);
    });
  }

  //valida que el valor sean unicamente números
  numbersValidator(control: AbstractControl): ValidationErrors | null {
    const value = control.value;

    const isNumber = /^\d+$/.test(value);
    return isNumber ? null : { onlyNumbers: true };
  }

  //impide que se puedan escribir caracteres que no sean números
  onInput(event: any): void {
    const inputValue = event.target.value;
    const numbersOnly = inputValue.replace(/[^0-9]/g, '');
    event.target.value = numbersOnly;
  }

  phoneNumberValidator(event: any): ValidationErrors | null {
    // Eliminar guiones antes de enviar el formulario
    const telefonoSinGuiones = event.value.replace(/-/g, '');

    if (telefonoSinGuiones.length === 10 && /^\d+$/.test(telefonoSinGuiones)) {
      return null; // El número de teléfono es válido
    } else {
      return { phoneNumber: true }; // El número de teléfono es inválido
    }
  }

  mensajeErrorNombre(): string {
    if (this.formGroup.get('nombre')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('nombre')?.hasError('pattern')) {
      return 'Nombre invalido';
    }
    return '';
  }

  mensajeErrorApellido(): string {
    if (this.formGroup.get('apellido')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('apellido')?.hasError('pattern')) {
      return 'Apellido invalido';
    }
    return '';
  }

  mensajeErrorEdad(): string {
    if (this.formGroup.get('edad')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('edad')?.hasError('onlyNumbers')) {
      return 'Solo se permiten números';
    }
    return '';
  }

  mensajeErrorTelefono(): string {
    if (this.formGroup.get('telefono')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('telefono')?.hasError('pattern')) {
      return 'Solo se permiten números y guiones';
    }

    return 'El numero es mayor a 10 dígitos';
  }

  mensajeErrorCorreo(): string {
    if (this.formGroup.get('correo')?.hasError('required')) {
      return 'Campo requerido';
    }
    if (this.formGroup.get('correo')?.hasError('email')) {
      return 'correo electrónico invalido';
    }
    return '';
  }

  onSubmit() {
    if (this.formGroup.valid) {
      this.usuarioService
        .putUsuario(this.idUsuario, this.formGroup.value)
        .subscribe(
          (data) => {
            console.log('res', data);
            this.mostrarNotificacion = true;
            setTimeout(() => {
              this.mostrarNotificacion = false;
            }, 3000);
          },
          (err) => {}
        );
    } else {
      console.log('not valid');
    }
  }
}
